package com.devcamp.task76_50.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task76_50.entity.Employee;

public interface IEmployeeRepository extends JpaRepository<Employee, Integer> {
    
}
