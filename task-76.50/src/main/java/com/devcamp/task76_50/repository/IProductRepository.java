package com.devcamp.task76_50.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task76_50.entity.Product;

public interface IProductRepository extends JpaRepository<Product, Integer> {
    
}
