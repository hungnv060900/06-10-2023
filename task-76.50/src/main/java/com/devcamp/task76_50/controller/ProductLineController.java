package com.devcamp.task76_50.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task76_50.entity.ProductLine;
import com.devcamp.task76_50.repository.IProductLineRepository;

@CrossOrigin
@RestController
@RequestMapping("/product-lines")
public class ProductLineController {
    @Autowired
    private IProductLineRepository pProductLineRepository;

    @GetMapping
    public ResponseEntity<List<ProductLine>> getAllProductLines() {
        try {
            List<ProductLine> productLines = pProductLineRepository.findAll();
            return new ResponseEntity<>(productLines, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductLine> getProductLineById(@PathVariable("id") int id) {
        Optional<ProductLine> productLineData = pProductLineRepository.findById(id);
        if (productLineData.isPresent()) {
            return new ResponseEntity<>(productLineData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<ProductLine> createProductLine(@Valid @RequestBody ProductLine productLine) {
        try {
            ProductLine createdProductLine = pProductLineRepository.save(productLine);
            return new ResponseEntity<>(createdProductLine, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProductLine> updateProductLineById(@PathVariable("id") int id,
            @Valid @RequestBody ProductLine updatedProductLine) {
        Optional<ProductLine> productLineData = pProductLineRepository.findById(id);
        if (productLineData.isPresent()) {
            ProductLine productLine = productLineData.get();
            productLine.setProductLine(updatedProductLine.getProductLine());
            productLine.setDescription(updatedProductLine.getDescription());
            productLine.setProducts(updatedProductLine.getProducts());
            try {
                ProductLine updatedProductLineData = pProductLineRepository.save(productLine);
                return new ResponseEntity<>(updatedProductLineData, HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteProductLineById(@PathVariable("id") int id) {
        try {
            pProductLineRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
