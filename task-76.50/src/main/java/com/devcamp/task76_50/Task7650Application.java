package com.devcamp.task76_50;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task7650Application {

	public static void main(String[] args) {
		SpringApplication.run(Task7650Application.class, args);
	}

}
